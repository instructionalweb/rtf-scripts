import React from 'react';
import styled from 'styled-components';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';

const ScriptDB = () => (
  <div>
    <NavBarStyling>
      <Navbar id="navBarStyle" expand="xl">
        <Navbar.Toggle />
        <Navbar.Collapse>
          <Nav className="mr-auto">
            {/* *** Toggle Views *** */}
            <Nav.Link onClick={this.toggleView} id="viewLink">
              {this.state.showCards ? 'List View' : 'Card View'}
            </Nav.Link>
            <Nav.Link
              onClick={this.state.showDate ? this.dateSort : this.alphaSort}
              id="sortLink"
            >
              {this.state.showAlpha ? 'Sort by Title' : 'Sort by Date'}
            </Nav.Link>
            {/* *** Reset to All *** */}
            {/* <h6>Search for Collaborators by:</h6> */}
            <Nav.Link onClick={this.resetForm} id="resetLink">
              All Scripts
            </Nav.Link>
            {/* *** sort by calls *** */}
            <NavDropdown title="Calls" id="callsDrop">
              <NavDropdown.Item title="Actors" onClick={e => this.callChange(e)}>
                Actors{' '}
              </NavDropdown.Item>
              <NavDropdown.Item title="Technical Crew" onClick={e => this.callChange(e)}>
                Technical Crew{' '}
              </NavDropdown.Item>
              <NavDropdown.Item
                title="Producers/Directors"
                onClick={e => this.callChange(e)}
              >
                Producers/Directors{' '}
              </NavDropdown.Item>
              <NavDropdown.Item title="Musicians" onClick={e => this.callChange(e)}>
                Musicians{' '}
              </NavDropdown.Item>
              <NavDropdown.Item title="Animators" onClick={e => this.callChange(e)}>
                Animators{' '}
              </NavDropdown.Item>
              <NavDropdown.Item title="Writers" onClick={e => this.callChange(e)}>
                Writers{' '}
              </NavDropdown.Item>
              <NavDropdown.Item title="Artists" onClick={e => this.callChange(e)}>
                Artists{' '}
              </NavDropdown.Item>
              <NavDropdown.Item title="Dancers" onClick={e => this.callChange(e)}>
                Dancers{' '}
              </NavDropdown.Item>
              <NavDropdown.Item title="Photographers" onClick={e => this.callChange(e)}>
                Photographers{' '}
              </NavDropdown.Item>
              <NavDropdown.Item
                title="Set, Scene, Jewelry Creators"
                onClick={e => this.callChange(e)}
              >
                Set, Scene, Jewelry Creators{' '}
              </NavDropdown.Item>
              <NavDropdown.Item title="Web Designers" onClick={e => this.callChange(e)}>
                Web Designers{' '}
              </NavDropdown.Item>
              <NavDropdown.Item
                title="Graphic/UX Designers"
                onClick={e => this.callChange(e)}
              >
                Graphic/UX Designers{' '}
              </NavDropdown.Item>
              <NavDropdown.Item title="Game Designers" onClick={e => this.callChange(e)}>
                Game Designers{' '}
              </NavDropdown.Item>
              <NavDropdown.Item
                title="Extended Reality Creators"
                onClick={e => this.callChange(e)}
              >
                Extended Reality Creators{' '}
              </NavDropdown.Item>
              <NavDropdown.Item title="Other Talent" onClick={e => this.callChange(e)}>
                Other Talent{' '}
              </NavDropdown.Item>
            </NavDropdown>
            {/* ***sort by category*** */}
            <NavDropdown title="Story/Project Form" id="categoryDrop">
              {/* Filter by Category */}
              <NavDropdown.Item
                title="Short Screenplay (under 5 pages)"
                onClick={e => this.categoryChange(e)}
              >
                Short Screenplay (under 5 pages){' '}
              </NavDropdown.Item>
              <NavDropdown.Item
                title="Short Screenplay (under 10 pages)"
                onClick={e => this.categoryChange(e)}
              >
                Short Screenplay (under 10 pages){' '}
              </NavDropdown.Item>
              <NavDropdown.Item
                title="Short Screenplay (over 10 pages)"
                onClick={e => this.categoryChange(e)}
              >
                Short Screenplay (over 10 pages){' '}
              </NavDropdown.Item>
              <NavDropdown.Item
                title="Short Screenplay, Animation"
                onClick={e => this.categoryChange(e)}
              >
                Short Screenplay, Animation{' '}
              </NavDropdown.Item>
              <NavDropdown.Item
                title="Feature Length Screenplay"
                onClick={e => this.categoryChange(e)}
              >
                Feature Length Screenplay{' '}
              </NavDropdown.Item>
              <NavDropdown.Item
                title="Digital Series Screenplay"
                onClick={e => this.categoryChange(e)}
              >
                Digital Series Screenplay{' '}
              </NavDropdown.Item>
              <NavDropdown.Item
                title="TV Pilot Teleplay"
                onClick={e => this.categoryChange(e)}
              >
                TV Pilot Teleplay{' '}
              </NavDropdown.Item>
              <NavDropdown.Item
                title="Podcast/Radio Script"
                onClick={e => this.categoryChange(e)}
              >
                Podcast/Radio Script{' '}
              </NavDropdown.Item>
              <NavDropdown.Item
                title="Virtual Reality Film Script"
                onClick={e => this.categoryChange(e)}
              >
                Virtual Reality Film Script{' '}
              </NavDropdown.Item>
              <NavDropdown.Item
                title="Interactive/Immersive Script"
                onClick={e => this.categoryChange(e)}
              >
                Interactive/Immersive Script{' '}
              </NavDropdown.Item>
              <NavDropdown.Item
                title="Augmented Reality Script"
                onClick={e => this.categoryChange(e)}
              >
                Augmented Reality Script{' '}
              </NavDropdown.Item>
              <NavDropdown.Item
                title="Mixed Reality Script"
                onClick={e => this.categoryChange(e)}
              >
                Mixed Reality Script{' '}
              </NavDropdown.Item>
              <NavDropdown.Item
                title="Comic Book/Graphic Novel Script"
                onClick={e => this.categoryChange(e)}
              >
                Comic Book/Graphic Novel Script{' '}
              </NavDropdown.Item>
              <NavDropdown.Item title="Game Script" onClick={e => this.categoryChange(e)}>
                Game Script{' '}
              </NavDropdown.Item>
              <NavDropdown.Item
                title="Theater Script"
                onClick={e => this.categoryChange(e)}
              >
                Theater Script{' '}
              </NavDropdown.Item>
              <NavDropdown.Item title="Short Story" onClick={e => this.categoryChange(e)}>
                Short Story{' '}
              </NavDropdown.Item>
              <NavDropdown.Item title="Novelette" onClick={e => this.categoryChange(e)}>
                Novelette{' '}
              </NavDropdown.Item>
              <NavDropdown.Item title="Novella" onClick={e => this.categoryChange(e)}>
                Novella{' '}
              </NavDropdown.Item>
              <NavDropdown.Item
                title="Visual Novel"
                onClick={e => this.categoryChange(e)}
              >
                Visual Novel{' '}
              </NavDropdown.Item>
              <NavDropdown.Item title="Poem" onClick={e => this.categoryChange(e)}>
                Poem{' '}
              </NavDropdown.Item>
              <NavDropdown.Item
                title="Transmedia Story"
                onClick={e => this.categoryChange(e)}
              >
                Transmedia Story{' '}
              </NavDropdown.Item>
              <NavDropdown.Item
                title="Other Mixed Media Arts Script"
                onClick={e => this.categoryChange(e)}
              >
                Other Mixed Media Arts Script{' '}
              </NavDropdown.Item>
              <NavDropdown.Item
                title="Music Video Concept"
                onClick={e => this.categoryChange(e)}
              >
                Music Video Concept{' '}
              </NavDropdown.Item>
              <NavDropdown.Item
                title="Synopsis/Treatment"
                onClick={e => this.categoryChange(e)}
              >
                Synopsis/Treatment{' '}
              </NavDropdown.Item>
            </NavDropdown>
            {/* *** project dates *** */}
            <NavDropdown title="Project Dates" id="projectDateDrop">
              <NavDropdown.Item
                title="Chronological"
                onClick={e => this.projectDateSort(e)}
              >
                Chronological{' '}
              </NavDropdown.Item>
              <NavDropdown.Item
                title="Reverse Chronological"
                onClick={e => this.projectDateRevSort(e)}
              >
                Reverse Chronological{' '}
              </NavDropdown.Item>
            </NavDropdown>
            {/* *** sort by genre *** */}
            <NavDropdown title="Genre" id="genreDrop">
              <NavDropdown.Item
                title="Action"
                onClick={e => this.filterChange(e)}
                // onClick={e => console.log(e.target.title)}
              >
                Action{' '}
              </NavDropdown.Item>
              <NavDropdown.Item title="Comedy" onClick={e => this.filterChange(e)}>
                Comedy{' '}
              </NavDropdown.Item>

              <NavDropdown.Item title="Dark Comedy" onClick={e => this.filterChange(e)}>
                Dark Comedy{' '}
              </NavDropdown.Item>
              <NavDropdown.Item title="Drama" onClick={e => this.filterChange(e)}>
                Drama{' '}
              </NavDropdown.Item>
              <NavDropdown.Item title="Experimental" onClick={e => this.filterChange(e)}>
                Experimental{' '}
              </NavDropdown.Item>
              <NavDropdown.Item title="Fantasy" onClick={e => this.filterChange(e)}>
                Fantasy{' '}
              </NavDropdown.Item>
              <NavDropdown.Item title="Historical" onClick={e => this.filterChange(e)}>
                Historical{' '}
              </NavDropdown.Item>
              <NavDropdown.Item title="Horror" onClick={e => this.filterChange(e)}>
                Horror{' '}
              </NavDropdown.Item>
              <NavDropdown.Item title="Musical/Dance" onClick={e => this.filterChange(e)}>
                Musical/Dance{' '}
              </NavDropdown.Item>
              <NavDropdown.Item
                title="Romantic Comedy"
                onClick={e => this.filterChange(e)}
              >
                Romantic Comedy{' '}
              </NavDropdown.Item>
              <NavDropdown.Item
                title="Science Fiction"
                onClick={e => this.filterChange(e)}
              >
                Science Fiction{' '}
              </NavDropdown.Item>
              <NavDropdown.Item title="Thriller" onClick={e => this.filterChange(e)}>
                Thriller{' '}
              </NavDropdown.Item>
              <NavDropdown.Item title="Other" onClick={e => this.filterChange(e)}>
                Other{' '}
              </NavDropdown.Item>
            </NavDropdown>
          </Nav>
          {/* ***search*** */}
          <Form inline onSubmit={this.handleSubmit}>
            <FormControl
              type="text"
              id="searchBox"
              onChange={this.handleChange}
              className="mr-sm-2"
            />{' '}
            {/* <a>x</a> */}
            <Button id="closeButton" type="reset" onClick={this.resetForm}>
              <i className="fas fa-window-close"></i>
            </Button>
          </Form>
        </Navbar.Collapse>
      </Navbar>
    </NavBarStyling>
    <ListWrapper>
      {/* ************ List ************ */}
      {!this.state.showCards && (
        <Table border hover responsive="md">
          <thead>
            <tr>
              <th>Project</th>
              <th>Genre</th>
              <th>Story/Project Form</th>
              <th>Date</th>
              <th>Creator</th>
              <th>Email</th>
              <th>Logline</th>
              <th>Calls for:</th>
              <th>Project Dates</th>
              <th>Collaborate</th>
            </tr>
          </thead>
          <tbody>
            {this.state.activeScripts.map((script, item) => (
              <tr key={item}>
                <td>
                  <a href={script.gsx$link.$t} target="_blank">
                    {script.gsx$title.$t}
                  </a>
                </td>
                <td>{script.gsx$genre.$t}</td>
                <td>{script.gsx$category.$t}</td>
                <td>{script.gsx$date.$t}</td>
                <td>{script.gsx$writer.$t}</td>
                <td>{script.gsx$email.$t}</td>
                <td>{script.gsx$description.$t}</td>
                <td>{script.gsx$cast.$t}</td>
                <td>{script.gsx$projectdates.$t}</td>
                {script.gsx$out.$t == '1' && (
                  <td>
                    {/* <a href="https://docs.google.com/forms/d/e/1FAIpQLScgUffzGbQSYwSyGBNJGwsXUbLCmN_xpeaHfDUpelTwqeqMpQ/viewform"> Start Collaboration </a> */}
                    <p>Collaboration Already Underway</p>
                  </td>
                )}
              </tr>
            ))}
          </tbody>
        </Table>
      )}
    </ListWrapper>
  </div>
);

export default ScriptDB;

const NavBarStyling = styled.div`
  font-family: 'Montserrat', sans-serif !important;
  color: rgb(245, 245, 245);
  font-weight: bold;
  &:hover {
    color: rgb(193, 216, 47);
  }
#navBarStyle {
    background-color: rgb(103, 135, 183);
    // color: red;
  }
  #viewLink {
    color: rgb(245, 245, 245);
    &:hover {
      color: rgb(193, 216, 47);
    }
  }
  #sortLink {
    color: rgb(245, 245, 245);
    &:hover {
      color: rgb(193, 216, 47);
    }
  }
  #resetLink {
    color: rgb(245, 245, 245);
    &:hover {
      color: rgb(193, 216, 47);
    }
  }
  #categoryDrop {
    color: rgb(245, 245, 245);
    &:hover {
      color: rgb(193, 216, 47);
    }
  }
  #genreDrop {
    color: rgb(245, 245, 245);
    &:hover {
      color: rgb(193, 216, 47);
    }
  }
  #callsDrop {
    color: rgb(245, 245, 245);
    &:hover {
      color: rgb(193, 216, 47);
    }
  }
  #projectDateDrop {
    color: rgb(245, 245, 245);
    &:hover {
      color: rgb(193, 216, 47);
    }
  }
#closeButton {
  border-style: solid;
  background-color: rgb(103, 135, 183);
  border-color: rgb(103, 135, 183);
  font-weight: bold;
  font-family: 'Montserrat', sans-serif !important;
  &:hover {
   color: rgb(193, 216, 47);
   border-style: solid;
   border-color: rgb(193, 216, 47);
  }
  &: focus {
    border-style: solid;
    background-color: rgb(103, 135, 183);
    border-color: rgb(103, 135, 183);
  }
}
  #searchBox {
    width: 50%;
  &:focus {
    outline: solid;
    border: 1px solid rgb(193, 216, 47);
    // background-color: rgb(0, 47, 108);
    box-shadow: 0 0 10px rgb(0, 47, 108);
    // width: 100%;
  }
  // &:hover {
  //   background-color: rgb(193, 216, 47);
  //  }
  }
}
`;
