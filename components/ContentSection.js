import React from 'react';
import styled from 'styled-components';

const ContentSection = () => (
  <Container>
    <h2>
      A system fostering cross-disciplinary creative development through collaboration.
    </h2>
    <Columns>
      <div>
        <h3>Access the Database</h3>
        <p>
          Students in <strong>any ACC class</strong> can participate.
        </p>
        <p>
          <strong>Students in ACC production/practices classes</strong> can search content
          with an eye towards collaboration.
        </p>
        <p>
          <strong>For project-based Arts &amp; Digital Media Division classes</strong>,
          students can search and discover content by various categories, projects ranging
          scripts to produce in a film or animation class, to a location-based AR
          experience proposal for a new media production class.
        </p>
        <p>
          <ol>
            <li>Get the password from your professor.</li>
            <li>Login below.</li>
          </ol>
        </p>
      </div>
      <div>
        <h3>Submit Content</h3>
        <p>
          Submit content such as scripts, plays, stories, music video treatments, game
          proposals, mixed media interactive theater projects, app proposals, or basically
          any story format for potential posting in the database.
        </p>
        <p>
          <strong>
            Class project “Calls” for Actors, Technical Crew or other talent can also be
            submitted!
          </strong>
        </p>
        <p>
          <ol>
            <li>
              If interested,{' '}
              <a
                href="https://docs.google.com/forms/d/e/1FAIpQLScKr8lQ8M63AVWgjGhW0MhA93jhKTed1OkVxyen6GFuY43RJw/viewform"
                target="_blank"
              >
                submit a project
              </a>{' '}
              to be included in the database.
            </li>
            <li>
              The “Co-labs Guide” will review your content. If accepted, you will be
              notified by email.
            </li>
          </ol>
        </p>
      </div>
      <div>
        <h3>Collaborate</h3>
        <p>Find a project in the database and want to collaborate?</p>
        <ol>
          <li>
            Interested students reach out directly to potential collaborators to develop a
            partnership.
          </li>
          <li>
            Students discuss/share project details and nature of collaboration and
            mutually decide to proceed.
          </li>
          <li>
            Both parties then sign a{' '}
            <a
              href="https://drive.google.com/file/d/1CFHWXe5Xel_m7WutvuYC7YwuCMwdNX0j/view?usp=sharing"
              target="_blank"
            >
              Collaborator Agreement PDF
            </a>
            . (Students agree to abide by principles of being a good collaborator
            throughout the process, to include assigning credit and sharing a copy of any
            final project for portfolios.)
          </li>
          <li>
            Email the Collaborator Agreement to{' '}
            <strong>professors of record in both the originating class</strong> and the
            production-oriented class, to secure their signatures.
          </li>
          <li>
            Submit your signed Collaboration Agreement PDF using this{' '}
            <a href="https://forms.gle/URxRTVntLqBLzFnD8" target="_blank">
              Google form
            </a>
            . The “Co-labs Guide” will confirm receipt. Then you may begin work on the
            project.
          </li>
        </ol>
      </div>
    </Columns>
    <p className="bottomContent">
      Option agreements may be developed (ie. a producing class or intellectual property
      class) but are arrangements made fully between students, which the college has no
      involvement with.
    </p>
    <p className="bottomContent">
      If a project is beyond the scope of a class collaboration, students at any time may
      remove their work and pursue their collaboration outside of class.
    </p>
  </Container>
);

export default ContentSection;

const Container = styled.div`
  h2 {
    font-size: 24px;
  }

  .bottomContent {
    font-size: 16px;
  }
`;

const Columns = styled.div`
  display: flex;
  gap: 15px 10px;
  width: 100%;
  margin-top: 3rem;
  margin-bottom: 2rem;

  p,
  ol {
    font-size: 14px !important;
  }

  ol {
    padding: 0;
    padding-left: 1rem;
  }

  a {
    font-weight: bold;
  }

  @media only screen and (max-width: 768px) {
    display: block;

    div {
      margin-bottom: 10px;
    }
  }

  div {
    flex: 1;
    background-color: #dcd9e1;
    padding: 1.5rem 2rem;

    h3 {
      padding-top: 0;
      padding-bottom: 16px;
    }
  }
`;
