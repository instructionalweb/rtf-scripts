// initial app creating and commit
// function HomePage() {
//  return <div>RTF Script Database</div>
// }
// export default HomePage
// resource: https://dev.to/iam_timsmith/lets-build-a-search-bar-in-react-120j

import React, { Component } from 'react';
import Form from 'react-bootstrap/Form';
import Container from 'react-bootstrap/Container';
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';
import Image from 'react-bootstrap/Image';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Table from 'react-bootstrap/Table';
import NavDropdown from 'react-bootstrap/NavDropdown';
import Navbar from 'react-bootstrap/Navbar';
import FormControl from 'react-bootstrap/FormControl';
import Nav from 'react-bootstrap/Nav';
import styled from 'styled-components';
import Axios from 'axios';
import Cookies from 'universal-cookie';
import ContentSection from '../components/ContentSection';
import { calls, storyProject, genres } from '../data/dropdowns';

export default class ScriptList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      // activeScripts: this.state.allScripts,
      // allScripts: this.state.allScripts,
      activeScripts: [],
      allScripts: [],
      completedScripts:[],
      showCards: true,
      // showAlpha: true,
      // showDate: false,
      loggedIn: false,
      logInFailed: false,
      password: '',
      rtfClass: 'Art',
      pwArray: [],
    };
    this.handleChange = this.handleChange.bind(this);
    // this.handleSubmit = this.handleSubmit.bind(this)
    this.filterChange = this.filterChange.bind(this);
    this.callChange = this.callChange.bind(this);
    this.projectDateSort = this.projectDateSort.bind(this);
    this.projectDateRevSort = this.projectDateRevSort.bind(this);
    // this.toggleView = this.toggleView.bind(this, view);
    this.logIn = this.logIn.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
    this.showCompletedScripts = this.showCompletedScripts.bind(this);
  }

  async getInitialData() {
    // Sheet Link - https://spreadsheets.google.com/feeds/list/1K6Uj6Druf1qCkcxmqbZdLekzhaK4od924k5mQytoNoY/1/public/values?alt=json
    // Password as of May 2021 - riverbatscolab

    const sheetId = '1K6Uj6Druf1qCkcxmqbZdLekzhaK4od924k5mQytoNoY';
    const apiKey = 'AIzaSyBJU4PeKulVbFBMrGUm9lSN3FwIdTQZQaE';
    let scripts = await Axios.get(
      `https://sheets.googleapis.com/v4/spreadsheets/${sheetId}/values/Submission?key=${apiKey}`
      // `https://spreadsheets.google.com/feeds/list/${sheetId}/1/public/values?alt=json`
    ).then(
      response => response.data.values
      // console.log("INSIDE SCRIPTS", response.data.feed.entry)
    );
    const indexOfApproved = scripts[0].findIndex( script => script === 'Approved');
    scripts.shift();
    scripts = scripts.filter(script => script[indexOfApproved] == '1');
    console.log("SCRIPTS#1: ", scripts);
    let pwArray = await Axios.get(
      `https://sheets.googleapis.com/v4/spreadsheets/${sheetId}/values/Passwords?key=${apiKey}`
      // `https://spreadsheets.google.com/feeds/list/${sheetId}/3/public/values?alt=json`
    ).then(
      response => response.data.values
    );
    pwArray.shift();
    scripts.forEach(script => (script[9] = script[9].split('=')[1])); // This gives us the unique key for the image so we can make it embedable
    console.log("SCRIPTS: ", scripts);

    //*********************Grab the Completed Project Sheet  ****************************
    let completedScripts = await Axios.get(
      `https://sheets.googleapis.com/v4/spreadsheets/${sheetId}/values/CompletedProjects?key=${apiKey}`
      // `https://spreadsheets.google.com/feeds/list/${sheetId}/1/public/values?alt=json`
    ).then(
      response => response.data.values
      // console.log("INSIDE SCRIPTS", response.data.feed.entry)
    );
    completedScripts.shift(); // This removes the heading row (eg Title, Name, etc) from the array
    completedScripts.forEach(script => (script[9] = script[9].split('=')[1])); // This gives us the unique key for the image so we can make it embedable

    this.setState(
      { activeScripts: scripts, allScripts: scripts, pwArray, password: "coolpw", completedScripts},
      this.alphaSort
    );
    this.setState({allScripts: this.state.activeScripts});
  }

  componentDidMount() {
    this.getInitialData();


    const cookies = new Cookies();
    if (cookies.get('rtf_logged_in') == 'true') {
      this.setState({ loggedIn: true });
      console.log('Logged in Automatically');
    }
  }

  logIn = e => {
    e.preventDefault();

    const cookies = new Cookies();
    this.state.pwArray.forEach(combo => {
      if(combo[0]==this.state.rtfClass && combo[1]==this.state.password){
        console.log('Login Successful');
        cookies.set('rtf_logged_in', 'true', { path: '/', maxAge: 2592000 });
        this.setState({ loggedIn: true });
      } else {
        this.setState({ logInFailed: true });
      }
    })

  };

  handleInputChange(event) {
    const { target } = event;
    const { value } = target;
    const { name } = target;

    this.setState({
      [name]: value,
    });
  }

  handleChange = e => {
    // console.log('form input')
    // console.log(e.target.value)
    e.preventDefault();
    // ***list of all scripts; original list***
    let scriptLibrary = [];
    // ***hold filtered list before state***
    let filteredScripts = [];
    // ***when search has input***
    if (e.target.value !== '') {
      // ***assign original list to this array***
      scriptLibrary = this.state.allScripts;
      // console.log(scriptLibrary)
      const filter = e.target.value.toLowerCase();
      // console.log(filter)
      // ***determine scripts to display based on search terms***
      filteredScripts = scriptLibrary.filter(
        script =>



      //     "Timestamp", // Keep on a line ending in 0 so that you can use your editors line numbers to count :)
      //     "When was the project created?",
      //     "Project Title",
      //     "Upload project description or script",
      //     "Project Genre",
      //     "Story/Project Form",
      //     "Person Submitting",
      //     "Email",
      //     "Project Logline",
      //     "Upload project image",
      //     "What class was the project created in?",
      //     "Class Professor's Name",
      //     "Dates of Project (if appropriate)",
      //     "Call for:",
      //     "",
      //     "",
      //     "",
      //     "Approved",
      //     "Out",
      //     "Corresponding Row (FOR TECH REASONS ONLY DO NOT EDIT)"
      // ]
          // console.log(script[8].includes(filter))
          script[1].toLowerCase().includes(filter) ||
          script[2].toLowerCase().includes(filter) ||
          script[3].toLowerCase().includes(filter) ||
          script[4].toLowerCase().includes(filter) ||
          script[5].toLowerCase().includes(filter) ||
          script[6].toLowerCase().includes(filter) ||
          script[8].toLowerCase().includes(filter) ||
          script[12].toLowerCase().includes(filter) ||
          script[13].toLowerCase().includes(filter)
      );
      // console.log(filteredScripts)
    } else {
      filteredScripts = this.state.activeScripts;
      // console.log(filteredScripts)
    }
    this.setState({
      activeScripts: filteredScripts,
    });
  };

  // ************** submit function **************
  handleSubmit = e => {
    // console.log('form input')
    e.preventDefault();
  };

  // ************** sort by alpha **************
  async alphaSort(e) {
    console.log("ALL SCRIPTS: ", this.state.allScripts);
    console.log("ACTIVE SCRIPTS: ", this.state.activeScripts);
    // console.log('link clicked')
    // ***list of all scripts; original list***
    let scriptData = [];
    // ***hold alpha sort list before state***
    let sortedScripts = [];
    // ***when search has input***
    if (sortedScripts !== '') {
      // ***assign original list to this array***
      scriptData = this.state.allScripts;
      // console.log(scriptData)
      const mapped = [...scriptData];
      // console.log(mapped)
      // ***sort***
      sortedScripts = mapped.sort((a, b) => {
        const alphaA = a[2];
        const alphaB = b[2];
        if (alphaA < alphaB) {
          return -1;
        }
        if (alphaA > alphaB) {
          return 1;
        }
        return 0;
        // console.log(mapped(sort))
      });
      this.setState({
        activeScripts: sortedScripts,
        showAlpha: !this.state.showAlpha,
        showDate: !this.state.showDate,
      });
    }
  };

  // ************** sort by date **************
  dateSort = () => {
    // console.log('link clicked')
    // ***list of all scripts; original list***
    let dateData = [];
    // ***hold alpha sort list before state***
    let orderedScripts = [];
    // ***when search has input***
    if (orderedScripts !== '') {
      // ***assign original list to this array***
      dateData = this.state.allScripts;
      const mappedDate = [...dateData];
      // console.log(mappedDate)
      // ***sort***
      orderedScripts = mappedDate.sort((a, b) => {
        const dateA = new Date(a[1]);
        const dateB = new Date(b[1]);
        if (dateA < dateB) {
          return -1;
        }
        if (dateA > dateB) {
          return 1;
        }
        return 0;
        // console.log(mappedDate(sort))
      });
      this.setState({
        activeScripts: orderedScripts,
        showDate: !this.state.showDate,
        showAlpha: !this.state.showAlpha,
      });
    }
  };

  // ************** sort by project date **************
  projectDateSort = () => {
    // console.log('link clicked')
    let projectDateData = [];
    let projectOrderedScripts = [];
    if (projectOrderedScripts !== '') {
      projectDateData = this.state.allScripts;
      const mappedProjectDate = [...projectDateData];
      // console.log(mappedProjectDate)
      // ***sort***
      projectOrderedScripts = mappedProjectDate.sort((a, b) => {
        const projectDateA = new Date(a[1]);
        const projectDateB = new Date(b[1]);
        if (projectDateA < projectDateB) {
          return -1;
        }
        if (projectDateA > projectDateB) {
          return 1;
        }
        return 0;
        // console.log(mappedProjectDate(sort))
      });
      this.setState({
        activeScripts: projectOrderedScripts,
      });
    }
  };

  // ************** sort by reverse chronological project date **************
  projectDateRevSort = () => {
    // console.log('link clicked')
    let projectDateRevData = [];
    let projectOrderedRevScripts = [];
    if (projectOrderedRevScripts !== '') {
      projectDateRevData = this.state.allScripts;
      const mappedProjectRevDate = [...projectDateRevData];
      // console.log(mappedProjectRevDate)
      // ***sort***
      projectOrderedRevScripts = mappedProjectRevDate.sort((a, b) => {
        const projectRevDateA = new Date(a[1]);
        const projectRevDateB = new Date(b[1]);
        if (projectRevDateA < projectRevDateB) {
          return 1;
        }
        if (projectRevDateA > projectRevDateB) {
          return -1;
        }
        return 0;
        // console.log(mappedProjectDate(sort))
      });
      this.setState({
        activeScripts: projectOrderedRevScripts,
        // chronDate: !this.state.chronDate,
        // revChronDate: !this.state.revChronDate,
      });
    }
  };

  // ************** reset state of form **************
  resetForm = () => {
    this.setState({ activeScripts: this.state.allScripts });
  };

  //  ************** genre filter **************
  filterChange = e => {
    // console.log('clicked');
    e.preventDefault();
    // console.log(e.target);
    // console.log(e.target.projecttitle);
    let allGenreData = [];
    let filteredGenreData = [];
    // when genre filter is triggered
    if (e.target.projecttitle !== '') {
      allGenreData = this.state.allScripts;
      // console.log(allGenreData)
      const genreFilter = e.target.title;
      //   // filteredGenreData = allGenreData.filter(script => script[4].includes('Thriller'));
      //   // console.log(filteredGenreData)
      //   // filteredGenreData = genreFilter.filter(script => {
      //   //   console.log(script.ç.includes(filteredGenreData))
      //   // })
      filteredGenreData = allGenreData.filter(script =>
        script[4].includes(genreFilter)
      );
      // console.log(filteredGenreData)
    }
    this.setState({
      activeScripts: filteredGenreData,
    });
  };

  // ************** category filter **************
  categoryChange = e => {
    // console.log('clicked');
    e.preventDefault();
    let allCategoryData = [];
    let filteredCategoryData = [];
    if (e.target.title !== '') {
      allCategoryData = this.state.allScripts;
      // console.log(allGenreData)
      const categoryFilter = e.target.title;
      filteredCategoryData = allCategoryData.filter(script =>
        script[5].includes(categoryFilter)
      );
      // console.log(filteredGenreData)
    }
    this.setState({
      activeScripts: filteredCategoryData,
    });
  };

  // ************** call filter **************
  callChange = e => {
    console.log('clicked');
    e.preventDefault();
    let allCallData = [];
    let filteredCallData = [];
    if (e.target.title !== '') {
      allCallData = this.state.allScripts;
      console.log(allCallData)
      const callFilter = e.target.title;
      console.log(e.target.title);
      filteredCallData = allCallData.filter(script =>
        script[13].includes(callFilter)
      );
      console.log(filteredCallData)
    }
    this.setState({
      activeScripts: filteredCallData,
    });
  };

  // ************** view toggle ****************

  toggleView = view => {
    // console.log('clicked')
    // console.log(this.state.showCards)
    const { showCards } = this.state;
    // console.log("INSIDE toggleView: showCards = ", showCards);
    if(view === 'list' && showCards || view === 'cards' && !showCards) {
      this.setState({
        showCards: !this.state.showCards,
      });
    }
  };

  //************Show Completed Projects ***************

  showCompletedScripts = e =>{
    e.preventDefault();
    this.setState({
      activeScripts: this.state.completedScripts,
    });
  }
  render() {
    // return <div>Hi, I am some content.</div>;
    return (
      <html>
        <head>
          {/* bootstrap   */}
          <link
            rel="stylesheet"
            href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
            integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk"
            crossOrigin="anonymous"
          />
          {/* custom css */}
          {/* <link rel="stylesheet" type="text/css" href="custom.css"></link> */}
          {/* fontawesome */}
          <script
            src="https://kit.fontawesome.com/276b5a8ef2.js"
            crossOrigin="anonymous"
          ></script>
          <meta name="viewport" content="width=device-width, initial-scale=1.0"></meta>
        </head>
        <header>
          <title>ACC Creative Co-Labs Connect</title>
          <Header>
            <ContainerStyling>
              <div>
                <Container>
                  <Row className="accLogoRow">
                    <Col>
                      <a href="https://www.austincc.edu/">
                        <img
                          alt="ACC Logo"
                          src="images/acc white logo.png"
                          id="logo"
                        ></img>
                      </a>
                      <h1>Creative Co-Labs Connect</h1>
                    </Col>
                  </Row>
                </Container>
              </div>
            </ContainerStyling>
          </Header>
        </header>
        <body>
          <div>
            <Container>
              <Intro>
                <div id="introDiv">
                  <Image src="images/RTFHeader.gif" alt="" id="headerImage" />
                </div>
                <ContentSection />
              </Intro>
              <div>
                <br />
                {/* Login Field */}
                {!this.state.loggedIn && (
                  <Login>
                    <p className="sign" align="center">
                      Sign in
                    </p>
                    <form className="form1" onSubmit={this.logIn}>
                      <select
                        id="rtfClasses"
                        value={this.state.rtfClass}
                        name="rtfClass"
                        onChange={this.handleInputChange}
                      >
                        {this.state.pwArray.map(className => (
                          <option value={className[0]}>{className[0]}</option>
                        ))}
                      </select>
                      <input
                        name="password"
                        className="pass"
                        type="password"
                        onChange={this.handleInputChange}
                        value={this.state.password}
                        align="center"
                        placeholder="Password"
                      />
                      <a className="submit" align="center" onClick={this.logIn}>
                        Sign in
                      </a>
                      {this.state.logInFailed && (
                        <p id="error">Login failed please try again</p>
                      )}
                      {/* <p class="forgot" align="center"><a href="#">Forgot Password?</a></p> */}
                    </form>
                  </Login>
                )}
                {this.state.loggedIn && (
                  <ScriptContainer>
                    <h2>Search for Collaborators:</h2>
                    <NavBarStyling>
                      <Navbar id="navBarStyle" expand="xl">
                        <Navbar.Toggle />
                        <Navbar.Collapse>
                          <Nav className="mr-auto">
                            {/* *** Toggle Views *** */}
                            {/* <Nav.Link onClick={this.toggleView} id="viewLink">
                              {this.state.showCards ? 'List View' : 'Card View'}
                            </Nav.Link> */}
                            <NavDropdown title="View">
                                <NavDropdown.Item
                                  title="List"
                                  onClick={ () => this.toggleView('list') }
                                >
                                  List
                                </NavDropdown.Item>
                                <NavDropdown.Item
                                  title="Card"
                                  onClick={ () => this.toggleView('cards')}
                                >
                                  Card
                                </NavDropdown.Item>
                            </NavDropdown>
                            {/* <Nav.Link
                              onClick={
                                this.state.showDate ? this.dateSort : this.alphaSort
                              }
                              id="sortLink"
                            >
                              {this.state.showAlpha ? 'Sort by Title' : 'Sort by Date'}
                            </Nav.Link> */}
                            {/* *** sort by calls *** */}
                            <NavDropdown title="Calls">
                              {calls.map(call => (
                                <NavDropdown.Item
                                  title={call}
                                  onClick={e => this.callChange(e)}
                                >
                                  {call}
                                </NavDropdown.Item>
                              ))}
                            </NavDropdown>
                            {/* ***sort by category*** */}
                            <NavDropdown title="Story/Project Form">
                              {storyProject.map(story => (
                                <NavDropdown.Item
                                  title={story}
                                  onClick={e => this.categoryChange(e)}
                                >
                                  {story}
                                </NavDropdown.Item>
                              ))}
                            </NavDropdown>
                            {/* *** project dates *** */}
                            <NavDropdown title="Project Dates">
                              <NavDropdown.Item
                                title="Chronological"
                                onClick={e => this.projectDateSort(e)}
                              >
                                Chronological{' '}
                              </NavDropdown.Item>
                              <NavDropdown.Item
                                title="Reverse Chronological"
                                onClick={e => this.projectDateRevSort(e)}
                              >
                                Reverse Chronological{' '}
                              </NavDropdown.Item>
                            </NavDropdown>
                            {/* *** sort by genre *** */}
                            <NavDropdown title="Genre">
                              {genres.map(genre => (
                                <NavDropdown.Item
                                  title={genre}
                                  onClick={e => this.filterChange(e)}
                                >
                                  {genre}
                                </NavDropdown.Item>
                              ))}
                            </NavDropdown>
                            {/* *** Reset to All *** */}
                            {/* <h6>Search for Collaborators by:</h6> */}
                            <Nav.Link onClick={this.resetForm}>
                              Clear Filters
                            </Nav.Link>
                            <Nav.Link onClick={this.showCompletedScripts}>
                              Completed Projects
                            </Nav.Link>
                            {/* ***search*** */}
                            <Form inline onSubmit={this.handleSubmit}>
                              <FormControl
                                type="text"
                                id="searchBox"
                                onChange={this.handleChange}
                                className="mr-sm-2"
                              />{' '}
                              {/* <a>x</a> */}
                              <Button
                                id="closeButton"
                                type="reset"
                                onClick={this.resetForm}
                              >
                                <i className="fas fa-window-close"></i>
                              </Button>
                            </Form>
                          </Nav>

                        </Navbar.Collapse>
                      </Navbar>
                    </NavBarStyling>
                    <ListWrapper>
                      {/* ************ List ************ */}
                      {!this.state.showCards && (
                        <Table border hover responsive>
                          <thead>
                            <tr>
                              <th>Project</th>
                              <th>Genre</th>
                              <th>Story/Project Form</th>
                              <th>Date</th>
                              <th>Creator</th>
                              <th>Email</th>
                              <th>Logline</th>
                              <th>Calls for:</th>
                              <th>Project Dates</th>
                              <th>Collaborate</th>
                            </tr>
                          </thead>
                          <tbody>
                            {this.state.activeScripts.map((script, item) => (
                              <tr key={item}>
                                <td>
                                  <a href={script[3]} target="_blank">
                                    {script[2]}
                                  </a>
                                </td>
                                <td>{script[4]}</td>
                                <td>{script[5]}</td>
                                <td>{script[1]}</td>
                                <td>{script[6]}</td>
                                <td id="list-view_email">{script[7]}</td>
                                <td>{script[8]}</td>
                                <td>{script[13]}</td>
                                <td>{script[12]}</td>
                                {(script[19] == 'X'|| script[19] == 'x' ) && (
                                  <td>
                                    <p>Collaboration Already Underway</p>
                                  </td>
                                )}
                              </tr>
                            ))}
                          </tbody>
                        </Table>
                      )}
                    </ListWrapper>
                  </ScriptContainer>
                )}

                <br />
                {this.state.loggedIn && (
                  <div>
                    {this.state.showCards && (
                      <CardWrapper>
                        {this.state.activeScripts.map((script, item) => (
                          // <Card id="cardStyling" style={{ width: '18rem' }}></Card>
                          <Card>
                            <Card.Body key={item}>
                            {(this.state.completedScripts!=this.state.activeScripts) && (
                              <Card.Subtitle className="mb-2 text-muted">
                                {script[1]}
                              </Card.Subtitle>
                            )}
                              <Card.Subtitle className="mb-2 text-muted">
                                {script[4]}
                              </Card.Subtitle>
                              <Card.Subtitle className="mb-2 text-muted">
                                {script[5]}
                              </Card.Subtitle>

                              <Card.Img
                                variant="top"
                                src={`https://lh3.googleusercontent.com/d/${script[9]}`}
                                id="cardImage"
                              />

                              <Card.Link href={script[3]} target="_blank">
                                {script[2]}
                              </Card.Link>
                              {(this.state.completedScripts==this.state.activeScripts) && (
                                <div className="text-center">
                                  <Card.Text>Completed Project</Card.Text>
                                </div>
                              )}
                              {/* <Card.Subtitle className="mb-2 text-muted">{script[5]}</Card.Subtitle> */}
                              {(this.state.completedScripts!=this.state.activeScripts) && (
                              <Card.Subtitle className="mb-2 text-muted" id="callsFor">
                                {' '}
                                Calls for: {script[13]}
                              </Card.Subtitle>
                               )}
                              {(script[12] !== '' && this.state.completedScripts!=this.state.activeScripts) && (
                                <Card.Text>
                                  Project Dates: {script[12]}
                                </Card.Text>
                              )}
                              {this.state.completedScripts!=this.state.activeScripts && (
                                <Card.Subtitle className="text-muted">
                                  {script[6]} - {script[7]}{' '}
                                </Card.Subtitle>
                              )}
                              {this.state.completedScripts==this.state.activeScripts && (
                                <Card.Text className="text-muted">
                                  Students: {script[6]}
                                </Card.Text>
                              )}
                              <Card.Text>{script[8]}</Card.Text>
                              {/* <div class="text-center"><Card.Link href={script[3]} target="_blank">Project</Card.Link></div> */}
                              {(script[14]!='' && this.state.completedScripts==this.state.activeScripts) && (
                                <Card.Text className="small-link mb-2">
                                  <strong>Watch: </strong><a href={script[14]}>{script[14]}</a>
                                </Card.Text>
                              )}
                              {this.state.completedScripts==this.state.activeScripts && (
                                <Card.Text className="small-link mb-2">
                                  <strong>Completed: </strong>{script[15]}
                                </Card.Text>
                              )}

                              {(script[19] == 'X'|| script[19] == 'x' ) && (
                                <div className="text-center">
                                  {/* <Card.Link href="https://docs.google.com/forms/d/e/1FAIpQLScgUffzGbQSYwSyGBNJGwsXUbLCmN_xpeaHfDUpelTwqeqMpQ/viewform" variant="success" target="_blank">Check Out</Card.Link> */}
                                  <Card.Text>Collaboration Already Underway</Card.Text>
                                </div>
                              )}
                            </Card.Body>
                          </Card>
                        ))}
                      </CardWrapper>
                    )}
                  </div>
                )}
              </div>
            </Container>
          </div>
        </body>
        <br />
        <br />
        <footer>
          <Footer>
            <Container>
              <Row>
                <Col>
                  <a href="#">
                    <img src="images/rtfLogo.png" alt=" " id="rtfLogo"></img>
                  </a>
                </Col>
                <Col className="social">
                  <h6>
                    <strong>Connect with Us</strong>
                  </h6>
                  <a href="https://twitter.com/ACCMBPT">
                    <i className="fab fa-twitter fa-3x" id="twitter"></i>
                  </a>
                  <a href="https://www.facebook.com/accmbpt/">
                    <i className="fab fa-facebook-f fa-3x" id="facebook"></i>
                  </a>
                  <a href="https://www.youtube.com/channel/UCefM-G_M2XDkbD_9TAPOwMg">
                    <i className="fab fa-youtube fa-3x" id="youtube"></i>
                  </a>
                </Col>
                <Col>
                  <h6>
                    <strong>Email Us</strong>
                  </h6>
                  <a href="#">
                    <i className="fas fa-envelope-square fa-3x" id="email"></i>
                  </a>
                </Col>
              </Row>
            </Container>
            <div id="tledFooter">
              <a href="#">
                <img src="images/tled white.png" id="tledLogo" alt=" "></img>
              </a>
            </div>
          </Footer>
        </footer>
      </html>
    );
  }
}


const Header = styled.div`
  margin: 0 auto;
  padding-top: 1rem;
  font-family: 'Montserrat', sans-serif !important;
  background-color: rgb(46, 26, 71);
  color: rgb(245, 245, 245);

  h1 {
    display: inline-block;
    padding-left: 20px;
  }

  #logo {
    height: 45px;
    width: 111px;
  }
`;

const Intro = styled.div`
  font-family: 'Montserrat', sans-serif !important;
  p {
    font-size: 20px;
  }
  h3,
  h2 {
    padding-top: 2rem;
    text-align: center;
  }
  a {
    color: rgb(79, 38, 130);
  }
  #headerImage {
    width: 100%;
    height: 450px;
    object-fit: cover;
    margin-bottom: 1.6rem;
  }
`;

const Wrapper = styled.div`
  display: flex;
  float: center;
  width: 90%;
  height: 90%;
  flex-direction: row;
  // align-items: center;
  justify-content: space-between;
  margin: 0 auto;
  font-family: 'Montserrat', sans-serif !important;
  // background-color: rgb(79, 38, 130);
  // color: rgb(245, 245, 245);
`;

const ListWrapper = styled.div`
  font-family: 'Montserrat', sans-serif !important;
  a {
    color: rgb(79, 38, 130);
    font-weight: bold;
    // font-size: 20px;
    &:hover {
      color: rgb(193, 216, 47);
    };
  }
  div.table-responsive, table.table {
    transform:rotateX(180deg);
    -moz-transform:rotateX(180deg); /* Mozilla */
    -webkit-transform:rotateX(180deg); /* Safari and Chrome */
    -ms-transform:rotateX(180deg); /* IE 9+ */
    -o-transform:rotateX(180deg); /* Opera */
  }
  td#list-view_email {
    max-width: 150px;
    word-wrap: break-word;
  }
`;

const CardWrapper = styled.div`
  display: flex;
  flex-wrap: wrap;
  width: 100%;
  height: 100%;
  justify-content: space-evenly;
  justify-items: center;
  margin: 0 auto;
  font-family: 'Montserrat', sans-serif !important;

  .card {
    width: 31%;
    margin-bottom: 16px;

    .text-center {
      color: white;
      border-radius: 5px;
      background-color: #00857d;
      padding: 8px 0;
    }
  }

  .small-link{
    font-size: 14px;

    a{
      display:inline-block ;
      font-size: 14px;
      font-weight:400;
  }
}
  @media only screen and (max-width: 768px) {
    display: block;

    .card {
      width: 100%;
    }
  }

  @media only screen and (max-width: 980px) and (min-width: 769px) {
    .card {
      width: 46%;
    }
  }
  a {
    color: rgb(79, 38, 130);
    font-weight: bold;
    font-size: 20px;
    &:hover {
      color: rgb(193, 216, 47);
    }
    display: block;
    margin: 12px 0;
  }
  #cardImage {
    // position: absolute;
    object-fit: cover;
    width: 100%;
    height: 200px;
  }
  #callsFor {
    font-weight: bold;
  }
`;

const ScriptContainer = styled.div`
  h2 {
    font-weight: bold;
    padding-bottom: 1rem;
  }
`;

const NavBarStyling = styled.div`
  font-family: 'Montserrat', sans-serif !important;
  color: rgb(245, 245, 245);
  font-weight: bold;
  &:hover {
    color: rgb(193, 216, 47);
  }

  .navbar-nav {
    width: 100%;
  }

  .navbar-nav > * {
    text-align: center;
    flex: 1;
    flex-grow: 1;

    //  ########  Search Form #########

    &:last-child {
      flex: 1.6;
      display: flex;
      flex-direction: row;
      align-self: flex-end;

      #searchBox {
        flex: 6;
        width: 100%;
        margin-left: 8px;
        &:focus {
          outline: solid;
          border: 1px solid rgb(193, 216, 47);
          // background-color: rgb(0, 47, 108);
          box-shadow: 0 0 10px rgb(0, 47, 108);
        }
      }

      #closeButton {
        flex: 1;
        border-style: solid;
        background-color: rgb( 79, 38, 130);
        border-color: rgb( 79, 38, 130);
        font-weight: bold;
        font-family: 'Montserrat', sans-serif !important;
        &:hover {
        color: rgb(193, 216, 47);
        border-style: solid;
        border-color: rgb(193, 216, 47);
        }
        &:focus {
          border-style: solid;
          background-color: rgb(103, 135, 183);
          border-color: rgb(103, 135, 183);
        }
      }
    }
  }

  #navBarStyle {
    background-color: rgb( 79, 38, 130);
      // color: red;


    a.nav-link {
      color: rgb(245, 245, 245);
      &:hover {
        color: rgb(193, 216, 47);
      }
    }
  }
`;

const ContainerStyling = styled.div`
  img {
    float: left;
    width: 75px;
    height: 50px;
    width: 10%;
    height: 90%;
  }
`;

const Login = styled.div`
  background-color: #FFFFFF;
  width: 400px;
  height: 400px;
  margin: 2rem auto;
  border-radius: 1.5em;
  box-shadow: 0px 11px 35px 2px rgba(0, 0, 0, 0.14);
  .sign {
        padding-top: 40px;
        color: #4d1978;
        font-family: 'Ubuntu', sans-serif;
        font-weight: bold;
        font-size: 23px;
    }

    .un {
    width: 76%;
    color: rgb(38, 50, 56);
    font-weight: 700;
    font-size: 14px;
    letter-spacing: 1px;
    background: rgba(136, 126, 126, 0.04);
    padding: 10px 20px;
    border: none;
    border-radius: 20px;
    outline: none;
    box-sizing: border-box;
    border: 2px solid rgba(0, 0, 0, 0.02);
    margin-bottom: 50px;
    margin-left: 46px;
    text-align: center;
    margin-bottom: 27px;
    font-family: 'Ubuntu', sans-serif;
    }

    form.form1 {
        padding-top: 40px;
    }

    #rtfClasses, .pass {
            width: 76%;
    color: rgb(38, 50, 56);
    font-weight: 700;
    font-size: 14px;
    letter-spacing: 1px;
    background: rgba(136, 126, 126, 0.04);
    padding: 10px 20px;
    border: none;
    border-radius: 20px;
    outline: none;
    box-sizing: border-box;
    border: 2px solid rgba(0, 0, 0, 0.02);
    margin-bottom: 50px;
    margin-left: 46px;
    text-align: center;
    margin-bottom: 27px;
    font-family: 'Ubuntu', sans-serif;
    }


    .un:focus, .pass:focus {
        border: 2px solid rgba(0, 0, 0, 0.18) !important;

    }
    #error{
      text-align: center;
      padding-top: 2rem;
      color:darkred;
    }
    .submit {
      cursor: pointer;
        border-radius: 5em;
        color: #fff;
        background: linear-gradient(to right, #4d1978, #984cdb);
        border: 0;
        padding-left: 40px;
        padding-right: 40px;
        padding-bottom: 10px;
        padding-top: 10px;
        font-family: 'Ubuntu', sans-serif;
        margin-left: 35%;
        font-size: 13px;
        box-shadow: 0 0 20px 1px rgba(0, 0, 0, 0.04);
    }

    .forgot {
        text-shadow: 0px 0px 3px rgba(117, 117, 117, 0.12);
        color: #E1BEE7;
        padding-top: 15px;
    }

    a {
        text-shadow: 0px 0px 3px rgba(117, 117, 117, 0.12);
        color: #E1BEE7;
        text-decoration: none
    }

    @media (max-width: 600px) {
        .main {
            border-radius: 0px;
        }
`;
const Footer = styled.div`
  @media only screen and (max-width: 768px) {
    > .container > .row {
      display: block;

      .col {
        margin-bottom: 3rem;

        h6 {
          font-size: 2rem;
        }
      }
    }
  }
  .col,
  h6 {
    text-align: center;
  }

  h6 {
    margin-bottom: 12px;
  }

  .social {
    a {
      padding-right: 12px;
    }

    a:last-child {
      padding: 0;
    }
  }
  #tledFooter {
    background-color: rgb(79, 38, 130);
    height: 100px;
  }
  #tledLogo {
    display: flex;
    margin-left: auto;
    margin-right: auto;
    width: 10%;
    height: 90%;
  }
  #logoRow {
    margin-top: 100px;
    display: flex;
    // align-items: center;
    // justify-content: center;
    // margin-bottom: 100px;
  }
  #connectRow {
    display: flex;
    margin-left: auto;
    margin-right: auto;
  }
  #emailRow {
    display: flex;
    margin-left: auto;
    margin-right: auto;
  }
  #rtfLogo {
    // display: block;
    margin-left: auto;
    margin-right: auto;
    /* width: 50%;
    height: 50%; */
  }
  #twitter {
    // display: block;
    margin-left: auto;
    margin-right: auto;
    // width: 100%;
    // height: 90%;
    color: rgb(0, 47, 108);
  }
  #facebook {
    // display: block;
    margin-left: auto;
    margin-right: auto;
    // width: 100%;
    // height: 90%;
    color: rgb(0, 47, 108);
  }
  #youtube {
    // display: flex;
    margin-left: auto;
    margin-right: auto;
    // width: 100%;
    // height: 90%;
    color: rgb(0, 47, 108);
  }
  #email {
    // display: flex;
    margin-left: auto;
    margin-right: auto;
    width: 100%;
    height: 90%;
    color: rgb(0, 47, 108);
  }
  #getStartedFooter {
    background-color: rgb(46, 26, 71);
    display: flex;
    align-items: center;
    justify-content: center;
    align-content: center;
    gap: 10px 10px;
    height: 100px;
  }
  // #applyButton {
  //   // display: inline-block;
  //   // align-self:center;
  //   margin-left: auto;
  //   margin-right: auto;
  //   margin: 10px;
  //   border-style: solid;
  //   border-color: rgb(255,255,255);
  //   background-color: rgb(46, 26, 71);
  //   font-weight: bold;
  //   font-family: 'Montserrat', sans-serif !important;
  // }
  // #infoButton {
  //   // display: inline-block;
  //   // align-self:center;
  //   margin-left: auto;
  //   margin-right: auto;
  //   border-style: solid;
  //   border-color: rgb(255,255,255);
  //   background-color: rgb(46, 26, 71);
  //   font-weight: bold;
  //   font-family: 'Montserrat', sans-serif !important;
  // }
`;

const NavStyling = styled.nav`
  a {
    color: white;
  }
`;

