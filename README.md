# RTF Scripts

This project is a front end for a collaboration tool for RTF and other creative departments. It displays upcoming projects and advertises what roles they need. E.g. an upcoming short film needs a music director or a lead actor

### Basic Info

- RTF Scripts uses a [Google Sheet](https://docs.google.com/spreadsheets/d/1K6Uj6Druf1qCkcxmqbZdLekzhaK4od924k5mQytoNoY/edit?usp=sharing) as a backend to manage the [front end](http://rtf-scripts.s3-website-us-west-2.amazonaws.com/)
- Entries are submitted using the form attached to that Google Sheet and
- The Google sheet is sensitive to order, so first thing to check on any issues is if the client changed the order of the columns
- Using Google Drive as an image hosting service is a bit of work around and sometimes Google will change the URL structure to access the image

### Installing

**YOU MUST USE NODE VERSION 15** Newer versions will not work
Different node requirements are common, please install [Node Version Manager](https://github.com/nvm-sh/nvm) to switch between them if you don't have a node version controller
Additionally, the most recent versions of Next are not compatable with the project as constructed, so a few of the libraries are restriced to older versions

- Git clone repo
- cd repo
- npm install
- npm run dev

### Who do I talk to?

- Original author was Corin, but Robert Foster is the current manager
- Client is RTF, [Christian Raymond](mailto:craymond@austincc.edu)
